/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab_3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Admin
 */
public class CaculationUnitTest {
    
    public CaculationUnitTest() {
    }    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    // TDD Test Diven Development
    @Test
    public void testAdd_num1_2_num2_2_output_4(){
        int result = Caculation.add(2,2);
        assertEquals(4,result);
    }
    
    @Test
    public void testAdd_num1_3_num2_2_output_5(){
        int result = Caculation.add(3,2);
        assertEquals(5,result);
    }
    
    @Test
    public void testAdd_num1_100_num2_20_output_120(){
        int result = Caculation.add(100,20);
        assertEquals(120,result);
    }
}
