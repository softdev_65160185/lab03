/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab_3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Admin
 */
public class testWin {
    
    public testWin() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testcheckWinRow0_O_output_true(){
        char [][] table = {{'O','O','O'},{'-','-','-'},{'-','-','-'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckWinRow1_O_output_true(){
        char [][] table = {{'-','-','-'},{'O','O','O'},{'-','-','-'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckWinRow2_O_output_true(){
        char [][] table = {{'-','-','-'},{'-','-','-'},{'O','O','O'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckWinCol0_O_output_true(){
        char [][] table = {{'O','-','-'},{'O','-','-'},{'O','-','-'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckWinCol1_O_output_true(){
        char [][] table = {{'-','O','-'},{'-','O','-'},{'-','O','-'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckWinCol2_O_output_true(){
        char [][] table = {{'-','-','O'},{'-','-','O'},{'-','-','O'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckWinRow0_X_output_true(){
        char [][] table = {{'X','X','X'},{'-','-','-'},{'-','-','-'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckWinRow1_X_output_true(){
        char [][] table = {{'-','-','-'},{'X','X','X'},{'-','-','-'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckWinRow2_X_output_true(){
        char [][] table = {{'-','-','-'},{'-','-','-'},{'X','X','X'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckWinCol0_X_output_true(){
        char [][] table = {{'X','-','-'},{'X','-','-'},{'X','-','-'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckWinCol1_X_output_true(){
        char [][] table = {{'-','X','-'},{'-','X','-'},{'-','X','-'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckWinCol2_X_output_true(){
        char [][] table = {{'-','-','X'},{'-','-','X'},{'-','-','X'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckWinAskew_O_output_true(){
        char [][] table = {{'O','-','-'},{'-','O','-'},{'-','-','O'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckWinAskew2_O_output_true(){
        char [][] table = {{'-','-','O'},{'-','O','-'},{'O','-','-'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckWinAskew_X_output_true(){
        char [][] table = {{'X','-','-'},{'-','X','-'},{'-','-','X'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckWinAskew2_X_output_true(){
        char [][] table = {{'-','-','X'},{'-','X','-'},{'X','-','-'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testcheckDraw1(){
        char [][] table = {{'O','O','X'},{'X','X','O'},{'O','O','X'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw2(){
        char [][] table = {{'X','X','O'},{'O','O','X'},{'X','X','O'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw3(){
        char [][] table = {{'O','X','O'},{'X','O','O'},{'X','O','X'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw4(){
        char [][] table = {{'X','O','X'},{'O','X','X'},{'O','X','O'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw5(){
        char [][] table = {{'X','O','X'},{'O','X','X'},{'O','X','O'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw6(){
        char [][] table = {{'X','O','X'},{'O','X','X'},{'O','X','O'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw7(){
        char [][] table = {{'O','X','O'},{'O','X','X'},{'X','O','O'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw8(){
        char [][] table = {{'X','O','X'},{'X','O','O'},{'O','X','X'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw9(){
        char [][] table = {{'X','O','X'},{'X','O','O'},{'O','X','X'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw10(){
        char [][] table = {{'X','O','X'},{'X','X','O'},{'O','X','O'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw11(){
        char [][] table = {{'O','X','O'},{'O','O','X'},{'X','O','X'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw12(){
        char [][] table = {{'O','X','O'},{'X','X','O'},{'O','O','X'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw13(){
        char [][] table = {{'X','O','X'},{'O','O','X'},{'X','X','O'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw14(){
        char [][] table = {{'O','X','O'},{'X','X','O'},{'O','O','X'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw15(){
        char [][] table = {{'X','O','X'},{'O','O','X'},{'X','X','O'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw16(){
        char [][] table = {{'O','O','X'},{'X','O','O'},{'O','X','X'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw17(){
        char [][] table = {{'X','X','O'},{'O','X','X'},{'X','O','O'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw18(){
        char [][] table = {{'O','X','O'},{'X','X','O'},{'X','O','X'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw19(){
        char [][] table = {{'X','O','X'},{'O','O','X'},{'O','X','O'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw20(){
        char [][] table = {{'O','X','O'},{'O','X','X'},{'X','O','O'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw21(){
        char [][] table = {{'X','O','X'},{'X','O','O'},{'O','X','X'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw22(){
        char [][] table = {{'X','O','O'},{'O','X','X'},{'X','O','O'}};
        char currentPlayer = 'X';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw23(){
        char [][] table = {{'O','X','X'},{'X','O','O'},{'O','X','X'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw24(){
        char [][] table = {{'X','O','X'},{'X','O','O'},{'O','X','O'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    @Test
    public void testcheckDraw25(){
        char [][] table = {{'O','X','O'},{'O','X','X'},{'X','O','X'}};
        char currentPlayer = 'O';
        boolean result = Lab_3.haveWon(table,currentPlayer);
        assertEquals(false,result);
    }
    
    
}
